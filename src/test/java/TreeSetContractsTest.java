import org.junit.jupiter.api.Test;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import ch.usi.si.codelounge.jsicko.Contract;
import org.junit.jupiter.api.function.Executable;

class TreeSetContractsTest {
    @Test
    public void testSize() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();

        assertEquals(0, set.size());
        set.add(1);
        assertEquals(1, set.size());
        set.remove(1);
        assertEquals(0, set.size());
    }

    @Test
    public void testEmpty() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();

        assertTrue(set.isEmpty());
        set.add(1);
        set.add(2);
        assertFalse(set.isEmpty());
        set.remove(1);
        assertFalse(set.isEmpty());
    }

    @Test
    public void testContains() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();

        set.add(20);
        set.add(-15);

        assertTrue(set.contains(20));
        assertTrue(set.contains(-15));
        assertFalse(set.contains(10));

        set.remove(20);
        assertFalse(set.contains(20));

        assertThrows(NullPointerException.class, () -> set.contains(null));
    }

    @Test
    public void testIterator() {
        TreeSetContracts<String> set = new TreeSetContracts<>();

        set.add("Item1");
        set.add("Item2");
        set.add("Item3");
        set.add("Item4");
        set.add("Item5");

        assertEquals(5, set.size());

        Iterator<String> iterator = set.iterator();

        String previous = null;
        int itemCount = 0;

        while (iterator.hasNext())
        {
            String current = iterator.next();
            assertTrue(set.contains(current));

            if (Objects.nonNull(previous))
                assertTrue(previous.compareTo(current) < 0);

            previous = current;
            itemCount++;
        }

        assertEquals(5, itemCount);
        assertEquals(set.size(), itemCount);
    }

    @Test
    public void testToArray() {
        TreeSetContracts<String> set = new TreeSetContracts<>();

        set.add("Item1");
        set.add("Item2");
        set.add("Item3");
        set.add("Item4");
        set.add("Item5");
        assertEquals(5, set.size());

        Object[] array = set.toArray();

        assertNotNull(array);

        assertEquals(set.size(), array.length);

        for (int i = 1; i <= 5; i++)
        {
            assertEquals("Item" + i, array[i - 1]);
            assertTrue(set.contains(array[i - 1]));
            if(i < 5)
                assertTrue(((String) array[i - 1]).compareTo((String) array[i]) < 0);
        }
    }

    @Test
    public void testToArrayWithArray() {
        TreeSetContracts<String> set = new TreeSetContracts<>();

        set.add("Item1");
        set.add("Item2");

        String[] providedArray = new String[2];
        String[] resultArray = set.toArray(providedArray);

        assertSame(providedArray, resultArray);
        assertEquals("Item1", resultArray[0]);
        assertEquals("Item2", resultArray[1]);

        // If the passed array length is smaller than the set size
        // a different array with adjusted length will be returned
        providedArray = new String[1];
        resultArray = set.toArray(providedArray);

        assertNotSame(providedArray, resultArray);
        assertEquals("Item1", resultArray[0]);
        assertEquals("Item2", resultArray[1]);

        // If the passed array length is greater than the set size
        // the exceeding cells will be set to null
        providedArray = new String[3];
        resultArray = set.toArray(providedArray);

        assertSame(providedArray, resultArray);
        assertEquals("Item1", resultArray[0]);
        assertEquals("Item2", resultArray[1]);
        assertNull(resultArray[2]);

        // assertThrows(NullPointerException.class, () -> set.toArray((Object[]) null));
    }

    @Test
    public void testAdd() {
        TreeSetContracts<String> set = new TreeSetContracts<>();

        assertTrue(set.isEmpty());

        assertTrue(set.add("Item1"));
        assertTrue(set.add("Item2"));
        assertTrue(set.add("Item3"));

        assertEquals(3, set.size());

        assertFalse(set.add("Item2"));
        assertEquals(3, set.size());

        assertThrows(NullPointerException.class, () -> set.add(null));

        testOrder(set);
    }

    @Test
    public void testRemove() {
        TreeSetContracts<String> set = new TreeSetContracts<>();

        set.add("Item1");
        set.add("Item2");
        set.add("Item3");

        assertEquals(3, set.size());

        assertFalse(set.remove("Item4"));
        assertTrue(set.remove("Item2"));

        assertEquals(2, set.size());

        testOrder(set);
    }

    @Test
    public void testContainsAll() {
        TreeSetContracts<String> set = new TreeSetContracts<>();
        set.add("Item1");
        set.add("Item2");
        set.add("Item3");

        Collection<String> testCollection = Arrays.asList("Item1", "Item3");
        assertTrue(set.containsAll(testCollection)); // All elements are present

        testCollection = Arrays.asList("Item1", "Item4");
        assertFalse(set.containsAll(testCollection)); // Some elements are not present

        assertThrows(NullPointerException.class, () -> set.containsAll(Arrays.asList(null)));

        assertTrue(set.containsAll(Arrays.asList())); // Empty collection
    }

    @Test
    public void testRetainAll() {
        Collection<String> testCollection = new ArrayList<>();
        Collection<String> testCollection2 = new ArrayList<>();
        TreeSetContracts<String> set = new TreeSetContracts<>();

        testCollection.add("Item2");
        testCollection.add("Item3");

        testCollection2.add("Item4");
        testCollection2.add("Item5");

        set.add("Item1");
        set.addAll(testCollection);

        assertTrue(set.retainAll(testCollection));
        assertEquals(2, set.size());
        assertFalse(set.contains("Item1"));
        assertTrue(set.containsAll(testCollection));
        testOrder(set);

        assertTrue(set.retainAll(testCollection2));
        assertEquals(0, set.size());
        testOrder(set);

        assertThrows(NullPointerException.class, () -> set.retainAll(Arrays.asList(null)));
    }

    @Test
    public void testClear() {
        TreeSetContracts<String> set = new TreeSetContracts<>();
        set.add("Item1");
        set.add("Item2");
        set.add("Item3");

        set.clear();
        assertTrue(set.isEmpty()); // HashSet should be empty after clear
    }
    @Test
    public void testEquals() {
        TreeSetContracts<String> set = new TreeSetContracts<>();
        set.add("Item2");
        set.add("Item3");

        TreeSetContracts<String> set2 = new TreeSetContracts<>();
        set2.add("Item3");
        set2.add("Item2");

        assertTrue(set.equals(set2)); // Equals another HashSet with same elements

        set.remove("Item3");
        assertFalse(set.equals(set2)); // Not equals another HashSet with different elements
        assertFalse(set.equals("SomeString")); // Not equals an object of different type
        assertTrue(set.equals(set)); // Equals to itself
    }

    @Test
    public void testAddAll() {
        TreeSetContracts<String> set = new TreeSetContracts<>();
        Collection<String> toAdd = new ArrayList<>();
        toAdd.addAll(Arrays.asList("Item3", "Item1", "Item2"));

        assertTrue(set.addAll(toAdd));
        assertEquals(3, set.size());
        testOrder(set);

        assertFalse(set.addAll(toAdd));

        assertThrows(NullPointerException.class, () -> set.addAll(Arrays.asList(null)));
    }

    @Test
    public void testRemoveAll() {
        TreeSetContracts<String> set = new TreeSetContracts<>();
        set.add("Item1");
        set.add("Item2");
        set.add("Item3");
        set.add("Item4");

        Collection<String> toRemove = new ArrayList<>();
        toRemove.addAll(Arrays.asList("Item2", "Item4", "Item6"));

        assertTrue(set.removeAll(toRemove));
        assertEquals(2, set.size());
        assertFalse(set.contains("Item2"));
        assertFalse(set.contains("Item4"));
        testOrder(set);

        assertThrows(NullPointerException.class, () -> set.removeAll(Arrays.asList(null)));
    }

    @Test
    public void testSubSet() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        set.addAll(toAdd);

        // Test a valid subrange
        SortedSet<Integer> subSet = set.subSet(3, 7);
        TreeSetContracts<Integer> set2 = new TreeSetContracts<>();
        Collection<Integer> toAdd2 = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        set2.addAll(toAdd2);
        assertEquals(set2, subSet);

        // Test an empty subrange
        SortedSet<Integer> emptySubSet = set.subSet(3, 3);
        assertTrue(emptySubSet.isEmpty());

        // Test boundaries
        SortedSet<Integer> boundarySubSet = set.subSet(1, 10);
        TreeSetContracts<Integer> set3 = new TreeSetContracts<>();
        Collection<Integer> toAdd3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        set3.addAll(toAdd3);
        assertEquals(set3, boundarySubSet);

        assertThrows(IllegalArgumentException.class, () -> set.subSet(8, 2));
        assertThrows(NullPointerException.class, () -> set.subSet(1, null));

        // test precondition: check_parameters
        // Executable check_parameters = () -> set.subSet(21, 2);
        // assertThrows(Contract.PreconditionViolation.class, check_parameters);
    }

    @Test
    public void testHeadSet() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        set.addAll(toAdd);

        // Test a valid head set
        SortedSet<Integer> headSet = set.headSet(5);
        TreeSetContracts<Integer> set2 = new TreeSetContracts<>();
        Collection<Integer> toAdd2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        set2.addAll(toAdd2);
        assertEquals(set2, headSet);

        // Test head set with all elements
        SortedSet<Integer> fullHeadSet = set.headSet(11);
        assertEquals(set, fullHeadSet);

        // Test empty head set
        SortedSet<Integer> emptyHeadSet = set.headSet(1);
        assertTrue(emptyHeadSet.isEmpty());

        assertThrows(NullPointerException.class, () -> set.headSet(null));
    }

    @Test
    public void testTailSet() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        set.addAll(toAdd);

        // Test a valid tail set
        SortedSet<Integer> tailSet = set.tailSet(5);
        TreeSetContracts<Integer> set2 = new TreeSetContracts<>();
        Collection<Integer> toAdd2 = new ArrayList<>(Arrays.asList(5, 6, 7, 8, 9, 10));
        set2.addAll(toAdd2);
        assertEquals(set2, tailSet);

        // Test tail set with all elements
        SortedSet<Integer> fullTailSet = set.tailSet(1);
        assertEquals(set, fullTailSet);

        // Test empty tail set
        SortedSet<Integer> emptyTailSet = set.tailSet(11);
        assertTrue(emptyTailSet.isEmpty());

        assertThrows(NullPointerException.class, () -> set.headSet(null));
    }

    @Test
    public void testFirst() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(3, 1, 4));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(1), set.first());
        set.clear();

        assertTrue(set.isEmpty());
        assertThrows(NoSuchElementException.class, () -> set.first());
    }

    @Test
    public void testLast() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(3, 1, 4));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(4), set.last());
        set.clear();

        assertTrue(set.isEmpty());
        assertThrows(NoSuchElementException.class, () -> set.last());
    }

    @Test
    public void testLower() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(3), set.lower(5));
        assertNull(set.lower(1));
        assertNull(set.lower(0));

        assertThrows(NullPointerException.class, () -> set.lower(null));
    }

    @Test
    public void testFloor() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(3), set.floor(3));
        assertEquals(Integer.valueOf(3), set.floor(4));
        assertNull(set.floor(0));

        assertThrows(NullPointerException.class, () -> set.floor(null));
    }

    @Test
    public void testCeiling() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(3), set.ceiling(3));
        assertEquals(Integer.valueOf(5), set.ceiling(4));
        assertNull(set.ceiling(10));

        assertThrows(NullPointerException.class, () -> set.ceiling(null));
    }

    @Test
    public void testHigher() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(5), set.higher(3));
        assertNull(set.higher(9));
        assertNull(set.higher(10));

        assertThrows(NullPointerException.class, () -> set.higher(null));
    }

    @Test
    public void testPollFirst() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(1), set.pollFirst());
        assertFalse(set.contains(1));

        set.clear();
        assertNull(set.pollFirst());
    }

    @Test
    public void testPollLast() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);

        assertEquals(Integer.valueOf(9), set.pollLast());
        assertFalse(set.contains(9));

        set.clear();
        assertNull(set.pollLast());
    }

    @Test
    public void testComparator() {
        TreeSetContracts<Integer> setWithNaturalOrder = new TreeSetContracts<>();
        assertNull(setWithNaturalOrder.comparator());

        TreeSetContracts<Integer> setWithCustomOrder = new TreeSetContracts<>(Comparator.reverseOrder());
        assertNotNull(setWithCustomOrder.comparator());
    }

    @Test
    public void testDescendingSet() {
        TreeSetContracts<Integer> set = new TreeSetContracts<>();
        Collection<Integer> toAdd = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        set.addAll(toAdd);
        NavigableSet<Integer> descendingSet = set.descendingSet();

        assertEquals(Arrays.asList(9, 7, 5, 3, 1), new ArrayList<>(descendingSet));
        descendingSet.add(6); // Should reflect in the original set
        assertTrue(set.contains(6));
    }

    private <E extends Comparable<E>> void testOrder(TreeSetContracts<E> set)
    {
        E previous = null;
        for (E current : set)
        {
            if (previous != null)
                assertTrue(previous.compareTo(current) < 0);

            previous = current;
        }
    }
}