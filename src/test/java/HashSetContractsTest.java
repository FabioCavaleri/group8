import org.junit.jupiter.api.Test;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;
import ch.usi.si.codelounge.jsicko.Contract;
import org.junit.jupiter.api.function.Executable;


class HashSetContractsTest {
    @Test
    public void testSize() {
        HashSetContracts<Integer> set = new HashSetContracts<>();

        assertEquals(0, set.size());
        set.add(1);
        assertEquals(1, set.size());
        set.remove(1);
        assertEquals(0, set.size());
    }

    @Test
    public void testEmpty() {
        HashSetContracts<Integer> set = new HashSetContracts<>();

        assertTrue(set.isEmpty());
        set.add(1);
        set.add(2);
        assertFalse(set.isEmpty());
        set.remove(1);
        assertFalse(set.isEmpty());
    }

    @Test
    public void testContains() {
        HashSetContracts<Integer> set = new HashSetContracts<>();

        set.add(20);
        set.add(-15);
        set.add(null);

        assertTrue(set.contains(20));
        assertTrue(set.contains(-15));
        assertTrue(set.contains(null));
        assertFalse(set.contains(10));
    }

    @Test
    public void testIterator() {
        // Create a set with some elements
        HashSetContracts<String> set = new HashSetContracts<>();
        Iterator<String> it = set.iterator();

        assertFalse(it.hasNext());

        set.add("Element1");
        set.add("Element2");
        set.add("Element3");
        set.add(null);

        it = set.iterator();
        int itemCount = 0;

        while (it.hasNext())
        {
            String element = it.next();
            assertTrue(set.contains(element));
            itemCount++;
        }

        assertEquals(set.size(), itemCount);
    }

    @Test
    public void testToArray() {
        HashSetContracts<String> set = new HashSetContracts<>();

        Object[] array = set.toArray();
        assertTrue(array.length == 0);

        set.add("one");
        set.add("two");
        set.add("three");
        array = set.toArray();

        assertEquals(set.size(), array.length);
        assertEquals("one", array[0]);
        assertEquals("two", array[1]);
        assertEquals("three", array[2]);
    }

    @Test
    public void testToArrayWithArray() {
        HashSetContracts<String> set = new HashSetContracts<>();

        set.add("one");
        set.add("two");

        String[] providedArray = new String[2];
        String[] resultArray = set.toArray(providedArray);

        assertSame(providedArray, resultArray);
        assertEquals("one", resultArray[0]);
        assertEquals("two", resultArray[1]);

        // If the passed array length is smaller than the set size
        // a different array with adjusted length will be returned
        providedArray = new String[1];
        resultArray = set.toArray(providedArray);

        assertNotSame(providedArray, resultArray);
        assertEquals("one", resultArray[0]);
        assertEquals("two", resultArray[1]);

        // If the passed array length is greater than the set size
        // the exceeding cells will be set to null
        providedArray = new String[3];
        resultArray = set.toArray(providedArray);

        assertSame(providedArray, resultArray);
        assertEquals("one", resultArray[0]);
        assertEquals("two", resultArray[1]);
        assertNull(resultArray[2]);

        assertThrows(NullPointerException.class, () -> set.addAll(null));

        // test precondition: array_non_null
        // Executable array_non_null = () -> set.toArray((Object[]) null);
        // assertThrows(Contract.PreconditionViolation.class, array_non_null);
    }

    @Test
    public void testAdd() {
        HashSetContracts<String> set = new HashSetContracts<>();

        assertTrue(set.isEmpty());
        assertTrue(set.add("hello"));
        assertTrue(set.contains("hello"));
        assertEquals(1, set.size());

        assertFalse(set.add("hello"));
        assertEquals(1, set.size());

        assertTrue(set.add(null));
        assertTrue(set.contains("hello"));
        assertTrue(set.contains(null));
        assertEquals(2, set.size());
    }

    @Test
    public void testRemove() {
        HashSetContracts<String> set = new HashSetContracts<>();
        set.add("Item2");
        set.add(null);

        assertEquals(2, set.size());

        assertFalse(set.remove("Item4")); // Test removing non-existing item
        assertEquals(2, set.size());
        assertTrue(set.remove("Item2")); // Test removing existing item
        assertEquals(1, set.size());
        assertTrue(set.remove(null)); // Test removinga null existing item
        assertEquals(0, set.size());

        assertFalse(set.contains("Item2"));
        assertFalse(set.contains(null));
    }

    @Test
    public void testContainsAll() {
        HashSetContracts<String> set = new HashSetContracts<>();
        set.add("Item1");
        set.add("Item2");
        set.add("Item3");

        Collection<String> testCollection = Arrays.asList("Item1", "Item3");
        assertTrue(set.containsAll(testCollection)); // All elements are present

        testCollection = Arrays.asList("Item1", "Item4");
        assertFalse(set.containsAll(testCollection)); // Some elements are not present

        // If the passed collection is null then a NullPointerException should be raised
        assertThrows(NullPointerException.class, () -> set.containsAll(Arrays.asList(null)));
        assertThrows(NullPointerException.class, () -> set.containsAll(null));

        assertTrue(set.containsAll(Arrays.asList())); // Empty collection

        // test precondition: collection_non_null
        // Executable collection_non_null = () -> set.containsAll(null);
        // assertThrows(Contract.PreconditionViolation.class, collection_non_null);
    }

    @Test
    public void testAddAll() {
        HashSetContracts<String> set = new HashSetContracts<>();
        set.add("Item1");
        set.add("Item2");

        Collection<String> testCollection = new ArrayList<>();
        testCollection.add("Item2");
        testCollection.add("Item3");
        testCollection.add("Item4");

        assertEquals(2, set.size());
        assertTrue(set.addAll(testCollection)); // Adding new elements
        assertEquals(4, set.size()); // Only adds "Item3" and "Item4"

        assertTrue(set.containsAll(testCollection));
        assertTrue(set.contains("Item1"));
        assertFalse(set.addAll(testCollection)); // All the elements already exist
        assertEquals(4, set.size());

        // If the passed collection is null then a NullPointerException should be raised
        assertThrows(NullPointerException.class, () -> set.addAll(Arrays.asList(null)));
        assertThrows(NullPointerException.class, () -> set.addAll(null));

        // test precondition: collection_non_null
        // Executable collection_non_null = () -> set.addAll(null);
        // assertThrows(Contract.PreconditionViolation.class, collection_non_null);

        assertFalse(set.addAll(Arrays.asList())); // Adding an empty collection
    }

    @Test
    public void testRetainAll() {
        Collection<String> testCollection = new ArrayList<>();
        Collection<String> testCollection2 = new ArrayList<>();
        HashSetContracts<String> set = new HashSetContracts<>();

        testCollection.add("Item2");
        testCollection.add("Item3");

        testCollection2.add("Item4");
        testCollection2.add("Item5");

        set.add("Item1");
        set.addAll(testCollection);

        assertTrue(set.retainAll(testCollection));
        assertEquals(2, set.size());
        assertFalse(set.contains("Item1"));
        assertTrue(set.containsAll(testCollection));

        assertTrue(set.retainAll(testCollection2));
        assertEquals(0, set.size());

        // If the passed collection is null then a NullPointerException should be raised
        assertThrows(NullPointerException.class, () -> set.retainAll(Arrays.asList(null)));
        assertThrows(NullPointerException.class, () -> set.retainAll(null));

        // test precondition: collection_non_null
        // Executable collection_non_null = () -> set.retainAll(null);
        // assertThrows(Contract.PreconditionViolation.class, collection_non_null);
    }

    @Test
    public void testRemoveAll() {
        Collection<String> testCollection = new ArrayList<>();
        testCollection.add("Item2");
        testCollection.add("Item3");

        HashSetContracts<String> set = new HashSetContracts<>();

        set.add("Item1");
        set.addAll(testCollection);
        assertEquals(3, set.size());

        testCollection.add("Item4");

        assertTrue(set.removeAll(testCollection));
        assertTrue(set.contains("Item1"));
        assertFalse(set.contains("Item2"));
        assertEquals(1, set.size());

        // If the passed collection is null then a NullPointerException should be raised
        assertThrows(NullPointerException.class, () -> set.removeAll(Arrays.asList(null)));
        assertThrows(NullPointerException.class, () -> set.removeAll(null));

        // test precondition: collection_non_null
        // Executable collection_non_null = () -> set.removeAll(null);
        // assertThrows(Contract.PreconditionViolation.class, collection_non_null);
    }

    @Test
    public void testClear() {
        HashSetContracts<String> set = new HashSetContracts<>();
        set.add("Item1");
        set.add("Item2");

        set.clear();
        assertTrue(set.isEmpty()); // HashSet should be empty after clear
    }
    @Test
    public void testEquals() {
        HashSetContracts<String> set = new HashSetContracts<>();
        set.add("Item1");
        set.add("Item2");

        HashSetContracts<String> set2 = new HashSetContracts<>();
        set2.add("Item2");
        set2.add("Item1");

        assertTrue(set.equals(set2)); // Equals another HashSet with same elements

        set.remove("Item2");
        assertFalse(set.equals(set2)); // Not equals another HashSet with different elements
        assertFalse(set.equals("SomeString")); // Not equals an object of different type
        assertTrue(set.equals(set)); // Equals to itself
    }
}