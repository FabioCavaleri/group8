import ch.usi.si.codelounge.jsicko.Contract;
import org.jetbrains.annotations.Nullable;

import static ch.usi.si.codelounge.jsicko.ContractUtils.*;

import java.util.*;

public interface SortedSetContracts<E> extends SortedSet<E>, SetContracts<E>, Contract
{
    // This new version of the Set collection needs to keep its elements ordered (ascending order by default)
    // This invariant verifies that this characteristic holds during the collection lifecycle
    // The check_order() method relies on the compare_elements functions to compare the elements of the collection
    @Pure
    @Invariant
    default boolean check_order()
    {
        return forAllInts(0, size() - 1, i -> compare_elements((E) toArray()[i], (E) toArray()[i + 1]) < 0);
    }


    // This method helps to compare two elements with both the possible value of the comparator attribute
    // If it is null then the two elements are compared following their natural order, otherwise they are
    // compared using the Comparator instance associated to the collection
    default int compare_elements(E e1, E e2)
    {
        if(Objects.isNull(comparator()))
            return ((Comparator<? super E>) Comparator.naturalOrder()).compare(e1, e2);
        else
            return comparator().compare(e1, e2);
    }

    @Pure
    Comparator<? super E> comparator();

    /**
     * Returns a view of the portion of this set whose elements range
     * from {@code fromElement}, inclusive, to {@code toElement},
     * exclusive.  (If {@code fromElement} and {@code toElement} are
     * equal, the returned set is empty.)  The returned set is backed
     * by this set, so changes in the returned set are reflected in
     * this set, and vice-versa.  The returned set supports all
     * optional set operations that this set supports.
     *
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param fromElement low endpoint (inclusive) of the returned set
     * @param toElement high endpoint (exclusive) of the returned set
     * @return a view of the portion of this set whose elements range from
     *         {@code fromElement}, inclusive, to {@code toElement}, exclusive
     * @throws ClassCastException if {@code fromElement} and
     *         {@code toElement} cannot be compared to one another using this
     *         set's comparator (or, if the set has no comparator, using
     *         natural ordering).  Implementations may, but are not required
     *         to, throw this exception if {@code fromElement} or
     *         {@code toElement} cannot be compared to elements currently in
     *         the set.
     * @throws NullPointerException if {@code fromElement} or
     *         {@code toElement} is null and this set does not permit null
     *         elements
     * @throws IllegalArgumentException if {@code fromElement} is
     *         greater than {@code toElement}; or if this set itself
     *         has a restricted range, and {@code fromElement} or
     *         {@code toElement} lies outside the bounds of the range
     */
    @Pure
    //@Requires("check_parameters")
    @Ensures({"equal_parameters", "subSet_check_order", "subSet_illegal_arguments", "subSet_null_exception", "check_all_lesser_than", "check_all_greater_than"})
    SortedSet<E> subSet(E fromElement, E toElement);

    @Pure
    default boolean check_parameters(E fromElement, E toElement, Throwable raises)
    {
        return implies(compare_elements(fromElement, toElement) > 0, () -> raises instanceof IllegalArgumentException);
    }

    @Pure
    default boolean equal_parameters(E fromElement, E toElement, SortedSet<E> returns)
    {
        return implies(fromElement.equals(toElement), () -> returns.isEmpty());
    }

    @Pure
    default boolean subSet_check_order(SortedSet<E> returns)
    {
        return implies(Objects.nonNull(returns), () -> forAllInts(0, returns.size() - 1, i -> compare_elements((E) returns.toArray()[i], (E) returns.toArray()[i + 1]) < 0));
    }

    @Pure
    default boolean subSet_illegal_arguments(E fromElement, E toElement, Throwable raises)
    {
        return implies(raises instanceof IllegalArgumentException, () -> !contains(fromElement) || !contains(toElement) || compare_elements(fromElement, toElement) > 0);
    }

    @Pure
    default boolean subSet_null_exception(E fromElement, E toElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(fromElement) || Objects.isNull(toElement));
    }

    @Pure
    default boolean check_all_lesser_than(E toElement, SortedSet<E> returns)
    {
        return implies(Objects.nonNull(returns), () -> forAll(returns, e -> compare_elements(e, toElement) < 0));
    }

    @Pure
    default boolean check_all_greater_than(E fromElement, SortedSet<E> returns)
    {
        return implies(Objects.nonNull(returns), () -> forAll(returns, e -> compare_elements(e, fromElement) >= 0));
    }

    /**
     * Returns a view of the portion of this set whose elements are
     * strictly less than {@code toElement}.  The returned set is
     * backed by this set, so changes in the returned set are
     * reflected in this set, and vice-versa.  The returned set
     * supports all optional set operations that this set supports.
     *
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param toElement high endpoint (exclusive) of the returned set
     * @return a view of the portion of this set whose elements are strictly
     *         less than {@code toElement}
     * @throws ClassCastException if {@code toElement} is not compatible
     *         with this set's comparator (or, if the set has no comparator,
     *         if {@code toElement} does not implement {@link Comparable}).
     *         Implementations may, but are not required to, throw this
     *         exception if {@code toElement} cannot be compared to elements
     *         currently in the set.
     * @throws NullPointerException if {@code toElement} is null and
     *         this set does not permit null elements
     * @throws IllegalArgumentException if this set itself has a
     *         restricted range, and {@code toElement} lies outside the
     *         bounds of the range
     */
    @Pure
    @Ensures({"check_all_lesser_than", "headSet_illegal_arguments", "headSet_null_exception"})
    SortedSet<E> headSet(E toElement);

    @Pure
    default boolean headSet_illegal_arguments(E toElement, Throwable raises)
    {
        return implies(raises instanceof IllegalArgumentException, () -> !contains(toElement));
    }

    @Pure
    default boolean headSet_null_exception(E toElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(toElement));
    }

    /**
     * Returns a view of the portion of this set whose elements are
     * greater than or equal to {@code fromElement}.  The returned
     * set is backed by this set, so changes in the returned set are
     * reflected in this set, and vice-versa.  The returned set
     * supports all optional set operations that this set supports.
     *
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param fromElement low endpoint (inclusive) of the returned set
     * @return a view of the portion of this set whose elements are greater
     *         than or equal to {@code fromElement}
     * @throws ClassCastException if {@code fromElement} is not compatible
     *         with this set's comparator (or, if the set has no comparator,
     *         if {@code fromElement} does not implement {@link Comparable}).
     *         Implementations may, but are not required to, throw this
     *         exception if {@code fromElement} cannot be compared to elements
     *         currently in the set.
     * @throws NullPointerException if {@code fromElement} is null
     *         and this set does not permit null elements
     * @throws IllegalArgumentException if this set itself has a
     *         restricted range, and {@code fromElement} lies outside the
     *         bounds of the range
     */
    @Pure
    @Ensures({"check_all_greater_than", "tailSet_illegal_arguments", "tailSet_null_exception"})
    SortedSet<E> tailSet(E fromElement);

    @Pure
    default boolean tailSet_illegal_arguments(E fromElement, Throwable raises)
    {
        return implies(raises instanceof IllegalArgumentException, () -> !contains(fromElement));
    }

    @Pure
    default boolean tailSet_null_exception(E fromElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(fromElement));
    }

    /**
     * Returns the first (lowest) element currently in this set.
     *
     * @return the first (lowest) element currently in this set
     * @throws NoSuchElementException if this set is empty
     */
    @Pure
    @Ensures({"empty_set", "check_is_lowest", "check_lowest_position"})
    E first();

    @Pure
    default boolean empty_set(Throwable raises)
    {
        return implies(raises instanceof NoSuchElementException, () -> isEmpty());
    }

    @Pure
    default boolean check_is_lowest(E returns)
    {
        return forAll(this, e -> returns.equals(e) ? true : compare_elements(e, returns) > 0);
    }

    @Pure
    default boolean check_lowest_position(E returns)
    {
        return implies(!isEmpty(), () -> toArray()[0].equals(returns));
    }

    /**
     * Returns the last (highest) element currently in this set.
     *
     * @return the last (highest) element currently in this set
     * @throws NoSuchElementException if this set is empty
     */
    @Pure
    @Ensures({"empty_set", "check_is_highest", "check_highest_position"})
    E last();

    @Pure
    default boolean check_is_highest(E returns)
    {
        return forAll(this, e -> returns.equals(e) ? true : compare_elements(e, returns) < 0);
    }

    @Pure
    default boolean check_highest_position(E returns)
    {
        return implies(!isEmpty(), () -> toArray()[size() - 1].equals(returns));
    }

    @Pure
    @Ensures({"contains_elements", "same_size", "check_array_order"})
    Object[] toArray();

    @Pure
    default boolean check_array_order(Object[] returns)
    {
        return forAllInts(0, size() - 1, i -> compare_elements((E) returns[i], (E) returns[i + 1]) < 0);
    }

    @Pure
    @Ensures({"contains_elements", "exceeding_cells", "check_returned_array_order", "null_array_exception"})
    <T> T[] toArray(T[] a);

    @Pure
    default <T> boolean check_returned_array_order(T[] returns)
    {
        return forAllInts(0, size() - 1, i -> compare_elements((E) returns[i], (E) returns[i + 1]) < 0);
    }
}
