import ch.usi.si.codelounge.jsicko.Contract;
import static ch.usi.si.codelounge.jsicko.Contract.old;
import static ch.usi.si.codelounge.jsicko.ContractUtils.*;

import java.util.*;

/**
 * A collection that contains no duplicate elements.  More formally, sets
 * contain no pair of elements {@code e1} and {@code e2} such that
 * {@code e1.equals(e2)}, and at most one null element.  As implied by
 * its name, this interface models the mathematical <i>set</i> abstraction.
 *
 * <p>The {@code Set} interface places additional stipulations, beyond those
 * inherited from the {@code Collection} interface, on the contracts of all
 * constructors and on the contracts of the {@code add}, {@code equals} and
 * {@code hashCode} methods.  Declarations for other inherited methods are
 * also included here for convenience.  (The specifications accompanying these
 * declarations have been tailored to the {@code Set} interface, but they do
 * not contain any additional stipulations.)
 *
 * <p>The additional stipulation on constructors is, not surprisingly,
 * that all constructors must create a set that contains no duplicate elements
 * (as defined above).
 *
 * <p>Note: Great care must be exercised if mutable objects are used as set
 * elements.  The behavior of a set is not specified if the value of an object
 * is changed in a manner that affects {@code equals} comparisons while the
 * object is an element in the set.  A special case of this prohibition is
 * that it is not permissible for a set to contain itself as an element.
 *
 * <p>Some set implementations have restrictions on the elements that
 * they may contain.  For example, some implementations prohibit null elements,
 * and some have restrictions on the types of their elements.  Attempting to
 * add an ineligible element throws an unchecked exception, typically
 * {@code NullPointerException} or {@code ClassCastException}.  Attempting
 * to query the presence of an ineligible element may throw an exception,
 * or it may simply return false; some implementations will exhibit the former
 * behavior and some will exhibit the latter.  More generally, attempting an
 * operation on an ineligible element whose completion would not result in
 * the insertion of an ineligible element into the set may throw an
 * exception or it may succeed, at the option of the implementation.
 * Such exceptions are marked as "optional" in the specification for this
 * interface.
 *
 * <h2><a id="unmodifiable">Unmodifiable Sets</a></h2>
 * <p>The {@link Set#of(Object...) Set.of} and
 * {@link Set#copyOf Set.copyOf} static factory methods
 * provide a convenient way to create unmodifiable sets. The {@code Set}
 * instances created by these methods have the following characteristics:
 *
 * <ul>
 * <li>They are <a href="Collection.html#unmodifiable"><i>unmodifiable</i></a>. Elements cannot
 * be added or removed. Calling any mutator method on the Set
 * will always cause {@code UnsupportedOperationException} to be thrown.
 * However, if the contained elements are themselves mutable, this may cause the
 * Set to behave inconsistently or its contents to appear to change.
 * <li>They disallow {@code null} elements. Attempts to create them with
 * {@code null} elements result in {@code NullPointerException}.
 * <li>They are serializable if all elements are serializable.
 * <li>They reject duplicate elements at creation time. Duplicate elements
 * passed to a static factory method result in {@code IllegalArgumentException}.
 * <li>The iteration order of set elements is unspecified and is subject to change.
 * <li>They are <a href="../lang/doc-files/ValueBased.html">value-based</a>.
 * Programmers should treat instances that are {@linkplain #equals(Object) equal}
 * as interchangeable and should not use them for synchronization, or
 * unpredictable behavior may occur. For example, in a future release,
 * synchronization may fail. Callers should make no assumptions
 * about the identity of the returned instances. Factories are free to
 * create new instances or reuse existing ones.
 * <li>They are serialized as specified on the
 * <a href="{@docRoot}/serialized-form.html#java.util.CollSer">Serialized Form</a>
 * page.
 * </ul>
 *
 * <p>This interface is a member of the
 * <a href="{@docRoot}/java.base/java/util/package-summary.html#CollectionsFramework">
 * Java Collections Framework</a>.
 *
 * @param <E> the type of elements maintained by this set
 *
 * @author  Josh Bloch
 * @author  Neal Gafter
 * @see Collection
 * @see List
 * @see SortedSet
 * @see HashSet
 * @see TreeSet
 * @see AbstractSet
 * @see Collections#singleton(java.lang.Object)
 * @see Collections#EMPTY_SET
 * @since 1.2
 */

public interface SetContracts<E> extends CollectionContracts<E>, Set<E>, Contract
{
    @Pure
    @Invariant
    default boolean no_duplicates()
    {
        // Every instance of a Set must not contain duplicated elements during its lifecycle
        // We check that every element of the Set has one and only one occurrence in the collection
        return forAll(this, e -> count_occurrences(e) == 1);
    }

    // This method iterates over the whole collection to count the number of occurrences of the
    // parameter e
    private int count_occurrences(E e)
    {
        Iterator<E> it = iterator();
        int occurrences = 0;

        if(Objects.isNull(e))
        {
            while(it.hasNext())
            {
                if(Objects.isNull(it.next()))
                    occurrences++;
            }
        }
        else
        {
            while(it.hasNext())
            {
                if(e.equals(it.next()))
                    occurrences++;
            }
        }

        return occurrences;
    }

    // Modification Operations

    /**
     * Adds the specified element to this set if it is not already present
     * (optional operation).  More formally, adds the specified element
     * {@code e} to this set if the set contains no element {@code e2}
     * such that
     * {@code Objects.equals(e, e2)}.
     * If this set already contains the element, the call leaves the set
     * unchanged and returns {@code false}.  In combination with the
     * restriction on constructors, this ensures that sets never contain
     * duplicate elements.
     *
     * <p>The stipulation above does not imply that sets must accept all
     * elements; sets may refuse to add any particular element, including
     * {@code null}, and throw an exception, as described in the
     * specification for {@link Collection#add Collection.add}.
     * Individual set implementations should clearly document any
     * restrictions on the elements that they may contain.
     *
     * @param e element to be added to this set
     * @return {@code true} if this set did not already contain the specified
     *         element
     * @throws UnsupportedOperationException if the {@code add} operation
     *         is not supported by this set
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this set
     * @throws NullPointerException if the specified element is null and this
     *         set does not permit null elements
     * @throws IllegalArgumentException if some property of the specified element
     *         prevents it from being added to this set
     */
    @Ensures({"add_size_check", "add_result_check", "add_null_exception", "check_old_elements"})
    boolean add(E e);

    @Pure
    default boolean add_size_check(boolean returns)
    {
        return returns == (size() == (old(this).size() + 1));
    }

    @Pure
    default boolean add_result_check(E e, boolean returns)
    {
        return returns == (!old(this).contains(e) && contains(e));
    }

    // Check if the elements of the collection before the add method call are in the new version of the collection
    // The Set<E> interface does not guarantee that the order of the elements will be constant over the time
    @Pure
    default boolean check_old_elements()
    {
        return forAll(old(this), e -> contains(e));
    }

    // If a NullPointerException is raised then a null parameter must have been passed to the method
    @Pure
    default boolean add_null_exception(E e, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(e));
    }

    /**
     * Removes the specified element from this set if it is present
     * (optional operation).  More formally, removes an element {@code e}
     * such that
     * {@code Objects.equals(o, e)}, if
     * this set contains such an element.  Returns {@code true} if this set
     * contained the element (or equivalently, if this set changed as a
     * result of the call).  (This set will not contain the element once the
     * call returns.)
     *
     * @param o object to be removed from this set, if present
     * @return {@code true} if this set contained the specified element
     * @throws ClassCastException if the type of the specified element
     *         is incompatible with this set
     * (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this
     *         set does not permit null elements
     * (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this set
     */
    @Ensures({"remove_size_check", "remove_result_check", "null_exception", "remove_check_old_elements"})
    boolean remove(Object o);

    @Pure
    default boolean remove_size_check(boolean returns)
    {
        return returns == (size() == (old(this).size() - 1));
    }

    @Pure
    default boolean remove_result_check(Object o, boolean returns)
    {
        return returns == (old(this).contains(o) && !contains(o));
    }

    // Check if the new collections actually contains all the elements of the old version except the Object o
    @Pure
    default boolean remove_check_old_elements(Object o)
    {
        return forAll(old(this), e -> Objects.isNull(e) ? (Objects.isNull(o) != contains(e)) : (e.equals(o) != contains(e)));
    }
    // Bulk Operations
    /**
     * Adds all of the elements in the specified collection to this set if
     * they're not already present (optional operation).  If the specified
     * collection is also a set, the {@code addAll} operation effectively
     * modifies this set so that its value is the <i>union</i> of the two
     * sets.  The behavior of this operation is undefined if the specified
     * collection is modified while the operation is in progress.
     *
     * @param  c collection containing elements to be added to this set
     * @return {@code true} if this set changed as a result of the call
     *
     * @throws UnsupportedOperationException if the {@code addAll} operation
     *         is not supported by this set
     * @throws ClassCastException if the class of an element of the
     *         specified collection prevents it from being added to this set
     * @throws NullPointerException if the specified collection contains one
     *         or more null elements and this set does not permit null
     *         elements, or if the specified collection is null
     * @throws IllegalArgumentException if some property of an element of the
     *         specified collection prevents it from being added to this set
     * @see #add(Object)
     */
    @Ensures({"collection_null_exception", "check_size_addAll", "check_if_added", "check_old_elements"})
    boolean addAll(Collection<? extends E> c);

    @Pure
    default boolean check_size_addAll(boolean returns)
    {
        return returns == (size() > old(this).size());
    }

    // Check if all the elements of the collection that were not contained in the Set before the call are actually added to the new version of the Set
    @Pure
    default boolean check_if_added(Collection<? extends E> c)
    {
        return forAll(c, e -> contains(e));
}

    // Comparison and hashing

    /**
     * Compares the specified object with this set for equality.  Returns
     * {@code true} if the specified object is also a set, the two sets
     * have the same size, and every member of the specified set is
     * contained in this set (or equivalently, every member of this set is
     * contained in the specified set).  This definition ensures that the
     * equals method works properly across different implementations of the
     * set interface.
     *
     * @param o object to be compared for equality with this set
     * @return {@code true} if the specified object is equal to this set
     */
    @Pure
    @Ensures("check_equals")
    boolean equals(Object o);

    @Pure
    default boolean check_equals(Object o, boolean returns)
    {
        return returns == (o instanceof Set && ((Set<?>) o).size() == size() && containsAll((Collection<?>) o));
    }
}
