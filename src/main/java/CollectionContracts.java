import ch.usi.si.codelounge.jsicko.Contract;
import static ch.usi.si.codelounge.jsicko.Contract.old;
import static ch.usi.si.codelounge.jsicko.ContractUtils.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.IntFunction;

public interface CollectionContracts<E> extends Collection<E>, Contract
{
    @Pure
    @Invariant
    default boolean non_negative_size()
    {
        return (size() >= 0);
    }

    /**
     * Returns the number of elements in this collection.  If this collection
     * contains more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     *
     * @return the number of elements in this collection
     */
    @Pure
    // @Ensures("non_negative_size")
    int size();

    /**
     * Returns {@code true} if this collection contains no elements.
     *
     * @return {@code true} if this collection contains no elements
     */
    @Pure
    @Ensures("empty_collection")
    boolean isEmpty();

    @Pure
    default boolean empty_collection(boolean returns)
    {
        return (returns == (size() == 0));
    }

    /**
     * Returns {@code true} if this collection contains the specified element.
     * More formally, returns {@code true} if and only if this collection
     * contains at least one element {@code e} such that
     * {@code Objects.equals(o, e)}.
     *
     * @param o element whose presence in this collection is to be tested
     * @return {@code true} if this collection contains the specified
     *         element
     * @throws ClassCastException if the type of the specified element
     *         is incompatible with this collection
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this
     *         collection does not permit null elements
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>)
     */
    @Pure
    @Ensures({"element_exists", "null_exception"})
    boolean contains(Object o);

    // Implementing the following expression reported in the method Javadoc description: (o==null ? e==null : o.equals(e))
    @Pure
    default boolean element_exists(Object o, boolean returns)
    {
        return implies(returns, () -> exists(this, e -> Objects.isNull(o) ? Objects.isNull(e) : o.equals(e)));
    }

    // If a NullPointerException is raised then a null parameter must have been passed to the method
    @Pure
    default boolean null_exception(Object o, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(o));
    }

    /**
     * Returns an iterator over the elements in this collection.  There are no
     * guarantees concerning the order in which the elements are returned
     * (unless this collection is an instance of some class that provides a
     * guarantee).
     *
     * @return an {@code Iterator} over the elements in this collection
     */
    @Pure
    @Ensures("hasNext_ifNotEmpty")
    Iterator<E> iterator();

    // If the collection is not empty then the iterator must have a "next" element
    @Pure
    default boolean hasNext_ifNotEmpty(Iterator<E> returns)
    {
        return implies(!isEmpty(), () -> returns.hasNext());
    }

    /**
     * Returns an array containing all of the elements in this collection.
     * If this collection makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the elements in
     * the same order. The returned array's {@linkplain Class#getComponentType
     * runtime component type} is {@code Object}.
     *
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this collection.  (In other words, this method must
     * allocate a new array even if this collection is backed by an array).
     * The caller is thus free to modify the returned array.
     *
     * @apiNote
     * This method acts as a bridge between array-based and collection-based APIs.
     * It returns an array whose runtime type is {@code Object[]}.
     * Use {@link #toArray(Object[]) toArray(T[])} to reuse an existing
     * array, or use {@link #toArray(IntFunction)} to control the runtime type
     * of the array.
     *
     * @return an array, whose {@linkplain Class#getComponentType runtime component
     * type} is {@code Object}, containing all of the elements in this collection
     */
    @Pure
    @Ensures({"contains_elements", "same_size"})
    Object[] toArray();

    // Check if every element in the array (results) is actually contained in the collection
    // Together with the same_size method it verifies that the array contains every element of the collection
    // In the second parameter of the forAllInts function we used size() to adapt the clause also for the second
    // version of toArray(T[] a)
    @Pure
    default boolean contains_elements(Object[] returns)
    {
        return forAllInts(0, size() - 1, i -> contains(returns[i]));
    }

    @Pure
    default boolean same_size(Object[] returns)
    {
        return (size() == returns.length);
    }

    /**
     * Returns an array containing all of the elements in this collection;
     * the runtime type of the returned array is that of the specified array.
     * If the collection fits in the specified array, it is returned therein.
     * Otherwise, a new array is allocated with the runtime type of the
     * specified array and the size of this collection.
     *
     * <p>If this collection fits in the specified array with room to spare
     * (i.e., the array has more elements than this collection), the element
     * in the array immediately following the end of the collection is set to
     * {@code null}.  (This is useful in determining the length of this
     * collection <i>only</i> if the caller knows that this collection does
     * not contain any {@code null} elements.)
     *
     * <p>If this collection makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the elements in
     * the same order.
     *
     * @apiNote
     * This method acts as a bridge between array-based and collection-based APIs.
     * It allows an existing array to be reused under certain circumstances.
     * Use {@link #toArray()} to create an array whose runtime type is {@code Object[]},
     * or use {@link #toArray(IntFunction)} to control the runtime type of
     * the array.
     *
     * <p>Suppose {@code x} is a collection known to contain only strings.
     * The following code can be used to dump the collection into a previously
     * allocated {@code String} array:
     *
     * <pre>
     *     String[] y = new String[SIZE];
     *     ...
     *     y = x.toArray(y);</pre>
     *
     * <p>The return value is reassigned to the variable {@code y}, because a
     * new array will be allocated and returned if the collection {@code x} has
     * too many elements to fit into the existing array {@code y}.
     *
     * <p>Note that {@code toArray(new Object[0])} is identical in function to
     * {@code toArray()}.
     *
     * @param <T> the component type of the array to contain the collection
     * @param a the array into which the elements of this collection are to be
     *        stored, if it is big enough; otherwise, a new array of the same
     *        runtime type is allocated for this purpose.
     * @return an array containing all of the elements in this collection
     * @throws ArrayStoreException if the runtime type of any element in this
     *         collection is not assignable to the {@linkplain Class#getComponentType
     *         runtime component type} of the specified array
     * @throws NullPointerException if the specified array is null
     */
    @Pure
    @Ensures({"contains_elements", "exceeding_cells", "null_array_exception"})
    <T> T[] toArray(T[] a);

    // If a NullPointerException is raised then a null array must have been passed to the method
    @Pure
    default <T> boolean null_array_exception(T[] a, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.nonNull(a));
    }

    // If the passed array length is greater than the collection size the remaining cells will be set to null
    @Pure
    default <T> boolean exceeding_cells(T[] returns)
    {
        return implies(returns.length > size(), () -> forAllInts(size(), returns.length - 1, i -> Objects.isNull(returns[i])));
    }

    /**
     * Returns {@code true} if this collection contains all of the elements
     * in the specified collection.
     *
     * @param  c collection to be checked for containment in this collection
     * @return {@code true} if this collection contains all of the elements
     *         in the specified collection
     * @throws ClassCastException if the types of one or more elements
     *         in the specified collection are incompatible with this
     *         collection
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified collection contains one
     *         or more null elements and this collection does not permit null
     *         elements
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null.
     * @see    #contains(Object)
     */
    @Pure
    @Ensures({"collection_null_exception", "all_elements_contained"})
    boolean containsAll(Collection<?> c);

    // If a NullPointerException is raised then either a collection containing a null element has been passed to the method
    @Pure
    default boolean collection_null_exception(Collection<?> c, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.nonNull(c) || exists(c, e -> Objects.isNull(e)));
    }

    @Pure
    default boolean all_elements_contained(Collection<?> c, boolean returns)
    {
        return returns == forAll(c, e -> contains(e));
    }

    /**
     * Removes all of this collection's elements that are also contained in the
     * specified collection (optional operation).  After this call returns,
     * this collection will contain no elements in common with the specified
     * collection.
     *
     * @param c collection containing elements to be removed from this collection
     * @return {@code true} if this collection changed as a result of the
     *         call
     * @throws UnsupportedOperationException if the {@code removeAll} method
     *         is not supported by this collection
     * @throws ClassCastException if the types of one or more elements
     *         in this collection are incompatible with the specified
     *         collection
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this collection contains one or more
     *         null elements and the specified collection does not support
     *         null elements
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */
    @Ensures({"collection_null_exception", "check_size_retainAll_removeAll", "check_if_removed"})
    boolean removeAll(Collection<?> c);

    @Pure
    default boolean check_if_removed(Collection<? extends E> c)
    {
        return forAll(old(this), e -> c.contains(e) ? !contains(e) : contains(e));
    }

    /**
     * Retains only the elements in this collection that are contained in the
     * specified collection (optional operation).  In other words, removes from
     * this collection all of its elements that are not contained in the
     * specified collection.
     *
     * @param c collection containing elements to be retained in this collection
     * @return {@code true} if this collection changed as a result of the call
     * @throws UnsupportedOperationException if the {@code retainAll} operation
     *         is not supported by this collection
     * @throws ClassCastException if the types of one or more elements
     *         in this collection are incompatible with the specified
     *         collection
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this collection contains one or more
     *         null elements and the specified collection does not permit null
     *         elements
     *         (<a href="{@docRoot}/java.base/java/util/Collection.html#optional-restrictions">optional</a>),
     *         or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */
    @Ensures({"collection_null_exception", "check_size_retainAll_removeAll", "check_if_retained"})
    boolean retainAll(Collection<?> c);

    @Pure
    default boolean check_size_retainAll_removeAll(boolean returns)
    {
        return returns == (size() < old(this).size());
    }

    @Pure
    default boolean check_if_retained(Collection<? extends E> c)
    {
        return forAll(old(this), e -> c.contains(e) ? contains(e) : !contains(e));
    }

    /**
     * Removes all of the elements from this collection (optional operation).
     * The collection will be empty after this method returns.
     *
     * @throws UnsupportedOperationException if the {@code clear} operation
     *         is not supported by this collection
     */
    @Ensures("collection_cleared")
    void clear();

    @Pure
    default boolean collection_cleared()
    {
        return isEmpty();
    }
}
