import org.jetbrains.annotations.NotNull;
import java.util.*;
import ch.usi.si.codelounge.jsicko.Contract;

import static ch.usi.si.codelounge.jsicko.ContractUtils.implies;

public class TreeSetContracts<E> extends TreeSet<E> implements NavigableSetContracts<E>
{
    public TreeSetContracts() { }

    public TreeSetContracts(Comparator<? super E> comparator) {
        super(comparator);
    }

    public TreeSetContracts(@NotNull Collection<? extends E> c) {
        super(c);
    }

    public TreeSetContracts(SortedSet<E> s) {
        super(s);
    }

    @Override
    public int size() { return super.size();}

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    @Ensures({"element_exists", "TreeSet_Object_null_exception"})
    public boolean contains(Object o) {
        return super.contains(o);
    }

    @Pure
    boolean TreeSet_Object_null_exception(Object o, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(o) && Objects.isNull(comparator()));
    }

    @Override
    public Iterator<E> iterator(){ return super.iterator(); }

    @Override
    public Object[] toArray() {
        return super.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return super.toArray(a);
    }

    @Override
    @Ensures({"add_size_check", "add_result_check", "check_old_elements", "TreeSet_E_null_exception"})
    public boolean add(E e) {
        return super.add(e);
    }

    @Pure
    boolean TreeSet_E_null_exception(E e, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(e) && Objects.isNull(comparator()));
    }

    @Override
    @Ensures({"remove_size_check", "remove_result_check", "TreeSet_Object_null_exception", "remove_check_old_elements"})
    public boolean remove(Object o) { return super.remove(o); }

    @Override
    public boolean containsAll(Collection<?> c) { return super.containsAll(c); }

    @Override
    @Ensures({"addAll_null_exception", "check_size_addAll", "check_if_added", "check_old_elements"})
    public boolean addAll(Collection<? extends E> c) { return super.addAll(c); }

    @Pure
    boolean addAll_null_exception(Collection<? extends E> c, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(c) || (c.contains(null) && Objects.isNull(comparator())));
    }

    @Override
    public boolean retainAll(Collection<?> c) { return super.retainAll(c); }

    @Override
    public boolean removeAll(Collection<?> c) { return super.removeAll(c); }

    @Override
    public void clear() { super.clear(); }

    @Override
    public boolean equals(Object o) { return super.equals(o); }

    @Override
    @Ensures({"equal_parameters", "subSet_check_order", "subSet_illegal_arguments", "subSet_new_null_exception", "check_all_lesser_than", "check_all_greater_than"})
    public SortedSet<E> subSet(E fromElement, E toElement) {
        return super.subSet(fromElement, toElement);
    }

    @Pure
    boolean subSet_new_null_exception(E fromElement, E toElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> (Objects.isNull(fromElement) || Objects.isNull(toElement) && Objects.isNull(comparator())));
    }

    @Override
    @Ensures({"check_all_lesser_than", "headSet_illegal_arguments", "headSet_new_null_exception"})
    public SortedSet<E> headSet(E toElement) { return super.headSet(toElement); }

    @Pure
    boolean headSet_new_null_exception(E toElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(toElement) && Objects.isNull(comparator()));
    }

    @Override
    @Ensures({"check_all_greater_than", "tailSet_illegal_arguments", "tailSet_new_null_exception"})
    public SortedSet<E> tailSet(E fromElement) { return super.tailSet(fromElement); }

    @Pure
    boolean tailSet_new_null_exception(E fromElement, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(fromElement) && Objects.isNull(comparator()));
    }

    @Override
    public E first() { return super.first(); }

    @Override
    public E last() { return super.last(); }

    @Override
    @Ensures({"check_lower", "TreeSet_E_null_exception"})
    public E lower(E e) { return super.lower(e); }

    @Override
    @Ensures({"check_floor", "TreeSet_E_null_exception"})
    public E floor(E e) { return super.floor(e); }

    @Override
    @Ensures({"check_ceiling", "TreeSet_E_null_exception"})
    public E ceiling(E e) { return super.ceiling(e); }

    @Override
    @Ensures({"check_higher", "TreeSet_E_null_exception"})
    public E higher(E e) { return super.higher(e); }

    @Override
    public E pollFirst() { return super.pollFirst(); }

    @Override
    public E pollLast() { return super.pollLast(); }

    @Override
    public Comparator<? super E> comparator() { return super.comparator(); }

    @Override
    public NavigableSet<E> descendingSet() { return super.descendingSet(); }
}
