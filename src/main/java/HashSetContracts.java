import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class HashSetContracts<E> extends HashSet<E> implements SetContracts<E>
{
    public HashSetContracts() {
    }

    public HashSetContracts(@NotNull Collection<? extends E> c) {
        super(c);
    }

    public HashSetContracts(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public HashSetContracts(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public int size() { return super.size();}

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return super.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return super.iterator();
    }

    @Override
    public Object[] toArray() {
        return super.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return super.toArray(a);
    }

    @Override
    public boolean add(E e) {
        return super.add(e);
    }

    @Override
    public boolean remove(Object o) { return super.remove(o); }

    @Override
    public boolean containsAll(Collection<?> c) { return super.containsAll(c); }

    @Override
    public boolean addAll(Collection<? extends E> c) { return super.addAll(c); }

    @Override
    public boolean retainAll(Collection<?> c) { return super.retainAll(c); }

    @Override
    public boolean removeAll(Collection<?> c) { return super.removeAll(c); }

    @Override
    public void clear() { super.clear(); }

    @Override
    public boolean equals(Object o) { return super.equals(o); }
}
