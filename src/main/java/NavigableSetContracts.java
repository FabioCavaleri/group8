import ch.usi.si.codelounge.jsicko.Contract;
import static ch.usi.si.codelounge.jsicko.Contract.old;
import static ch.usi.si.codelounge.jsicko.ContractUtils.*;

import java.util.*;


/**
 * A {@link SortedSet} extended with navigation methods reporting
 * closest matches for given search targets. Methods {@link #lower},
 * {@link #floor}, {@link #ceiling}, and {@link #higher} return elements
 * respectively less than, less than or equal, greater than or equal,
 * and greater than a given element, returning {@code null} if there
 * is no such element.
 *
 * <p>A {@code NavigableSet} may be accessed and traversed in either
 * ascending or descending order.  The {@link #descendingSet} method
 * returns a view of the set with the senses of all relational and
 * directional methods inverted. The performance of ascending
 * operations and views is likely to be faster than that of descending
 * ones.  This interface additionally defines methods {@link
 * #pollFirst} and {@link #pollLast} that return and remove the lowest
 * and highest element, if one exists, else returning {@code null}.
 * Methods
 * {@link #subSet(Object, boolean, Object, boolean) subSet(E, boolean, E, boolean)},
 * {@link #headSet(Object, boolean) headSet(E, boolean)}, and
 * {@link #tailSet(Object, boolean) tailSet(E, boolean)}
 * differ from the like-named {@code SortedSet} methods in accepting
 * additional arguments describing whether lower and upper bounds are
 * inclusive versus exclusive.  Subsets of any {@code NavigableSet}
 * must implement the {@code NavigableSet} interface.
 *
 * <p>The return values of navigation methods may be ambiguous in
 * implementations that permit {@code null} elements. However, even
 * in this case the result can be disambiguated by checking
 * {@code contains(null)}. To avoid such issues, implementations of
 * this interface are encouraged to <em>not</em> permit insertion of
 * {@code null} elements. (Note that sorted sets of {@link
 * Comparable} elements intrinsically do not permit {@code null}.)
 *
 * <p>Methods
 * {@link #subSet(Object, Object) subSet(E, E)},
 * {@link #headSet(Object) headSet(E)}, and
 * {@link #tailSet(Object) tailSet(E)}
 * are specified to return {@code SortedSet} to allow existing
 * implementations of {@code SortedSet} to be compatibly retrofitted to
 * implement {@code NavigableSet}, but extensions and implementations
 * of this interface are encouraged to override these methods to return
 * {@code NavigableSet}.
 *
 * <p>This interface is a member of the
 * <a href="{@docRoot}/java.base/java/util/package-summary.html#CollectionsFramework">
 * Java Collections Framework</a>.
 *
 * @author Doug Lea
 * @author Josh Bloch
 * @param <E> the type of elements maintained by this set
 * @since 1.6
 */
public interface NavigableSetContracts<E> extends NavigableSet<E>, SortedSetContracts<E>, Contract
{
    /**
     * Returns the greatest element in this set strictly less than the
     * given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the greatest element less than {@code e},
     *         or {@code null} if there is no such element
     * @throws ClassCastException if the specified element cannot be
     *         compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *         and this set does not permit null elements
     */
    @Pure
    @Ensures({"check_lower", "null_parameter_exception"})
    E lower(E e);

    @Pure
    default boolean check_lower(E e, E returns)
    {
        return Objects.nonNull(returns)
                    ? existsLower(e)
                    : !existsLower(e);
    }

    default boolean existsLower(E e) {
        return exists(this, i -> compare_elements(i, e) < 0 &&
                forAll(this, x -> (compare_elements(x, i) < 0) ||
                        compare_elements(x, e) > 0 ||
                        x.equals(e) ||
                        x.equals(i)));
    }

    @Pure
    default boolean null_parameter_exception(E e, Throwable raises)
    {
        return implies(raises instanceof NullPointerException, () -> Objects.isNull(e));
    }

    /**
     * Returns the greatest element in this set less than or equal to
     * the given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the greatest element less than or equal to {@code e},
     *         or {@code null} if there is no such element
     * @throws ClassCastException if the specified element cannot be
     *         compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *         and this set does not permit null elements
     */
    @Pure
    @Ensures({"check_floor", "null_parameter_exception"})
    E floor(E e);

    @Pure
    default boolean check_floor(E e, E returns)
    {
        return Objects.nonNull(returns)
                ? existsFloor(e)
                : !existsFloor(e);
    }

    default boolean existsFloor(E e) {
        return exists(this, i -> compare_elements(i, e) <= 0 &&
                forAll(this, x -> (compare_elements(x, i) < 0) ||
                        compare_elements(x, e) > 0 ||
                        x.equals(e) ||
                        x.equals(i)));
    }

    /**
     * Returns the least element in this set greater than or equal to
     * the given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the least element greater than or equal to {@code e},
     *         or {@code null} if there is no such element
     * @throws ClassCastException if the specified element cannot be
     *         compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *         and this set does not permit null elements
     */
    @Pure
    @Ensures({"check_ceiling", "null_parameter_exception"})
    E ceiling(E e);

    @Pure
    default boolean check_ceiling(E e, E returns)
    {
        return Objects.nonNull(returns)
                ? existsCeiling(e)
                : !existsCeiling(e);
    }

    default boolean existsCeiling(E e) {
        return exists(this, i -> compare_elements(i, e) >= 0 &&
                forAll(this, x -> (compare_elements(x, i) < 0) ||
                        compare_elements(x, e) > 0 ||
                        x.equals(e) ||
                        x.equals(i)));
    }

    /**
     * Returns the least element in this set strictly greater than the
     * given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the least element greater than {@code e},
     *         or {@code null} if there is no such element
     * @throws ClassCastException if the specified element cannot be
     *         compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *         and this set does not permit null elements
     */
    @Pure
    @Ensures({"check_higher", "null_parameter_exception"})
    E higher(E e);

    @Pure
    default boolean check_higher(E e, E returns)
    {
        return Objects.nonNull(returns)
                ? existsHigher(e)
                : !existsHigher(e);
    }

    default boolean existsHigher(E e) {
        return exists(this, i -> compare_elements(i, e) > 0 &&
                forAll(this, x -> (compare_elements(x, i) < 0) ||
                        compare_elements(x, e) > 0 ||
                        x.equals(e) ||
                        x.equals(i)));
    }

    /**
     * Retrieves and removes the first (lowest) element,
     * or returns {@code null} if this set is empty.
     *
     * @return the first element, or {@code null} if this set is empty
     */
    @Ensures({"empty_set_null_result", "check_pollFirst", "check_old_pollFirst"})
    E pollFirst();

    @Pure
    default boolean empty_set_null_result(E returns)
    {
        return implies(Objects.isNull(returns), () -> isEmpty());
    }

    @Pure
    default boolean check_pollFirst(E returns)
    {
        return implies(Objects.nonNull(returns), () -> old(this).toArray()[0].equals(returns) && !contains(returns));
    }

    @Pure
    default boolean check_old_pollFirst(E returns)
    {
        return implies(Objects.nonNull(returns), () -> forAllInts(1, old(this).size() - 1, i -> old(this).toArray()[i].equals(this.toArray()[i - 1])));
    }

    /**
     * Retrieves and removes the last (highest) element,
     * or returns {@code null} if this set is empty.
     *
     * @return the last element, or {@code null} if this set is empty
     */
    @Ensures({"empty_set_null_result", "check_pollLast", "check_old_pollLast"})
    E pollLast();

    @Pure
    default boolean check_pollLast(E returns)
    {
        return implies(Objects.nonNull(returns), () -> old(this).toArray()[size()].equals(returns) && !contains(returns));
    }

    @Pure
    default boolean check_old_pollLast(E returns)
    {
        return implies(Objects.nonNull(returns), () -> forAllInts(0, size() - 1, i -> old(this).toArray()[i].equals(toArray()[i])));
    }

    /**
     * Returns a reverse order view of the elements contained in this set.
     * The descending set is backed by this set, so changes to the set are
     * reflected in the descending set, and vice-versa.  If either set is
     * modified while an iteration over either set is in progress (except
     * through the iterator's own {@code remove} operation), the returns of
     * the iteration are undefined.
     *
     * <p>The returned set has an ordering equivalent to
     * {@link Collections#reverseOrder(Comparator) Collections.reverseOrder}{@code (comparator())}.
     * The expression {@code s.descendingSet().descendingSet()} returns a
     * view of {@code s} essentially equivalent to {@code s}.
     *
     * @return a reverse order view of this set
     */
    @Pure
    @Ensures("check_desc_order")
    NavigableSet<E> descendingSet();

    @Pure
    default boolean check_desc_order(NavigableSet<E> returns)
    {
        return implies(!returns.isEmpty(), () -> forAllInts(0, returns.size() - 1, i -> compare_elements((E) returns.toArray()[i], (E) returns.toArray()[i + 1]) > 0));
    }
}
